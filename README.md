# alpine-caddy
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-caddy)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-caddy)



----------------------------------------
### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-caddy/x64)
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-caddy/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : [Caddy](https://caddyserver.com/)
    - Caddy is the HTTP/2 web server with automatic HTTPS.



----------------------------------------
#### Run

```sh
docker run -d \
           -p 80:80/tcp \
           -p 443:443/tcp \
           -v /conf.d:/conf.d \
           -v /srv:srv \
           forumi0721/alpine-caddy:[ARCH_TAG]
```



----------------------------------------
#### Usage

* URL : [http://localhost:80/](http://localhost:80/)
    - If you want to use custom setting, you need to create `CaddyFile`.



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 80/tcp             | HTTP port                                        |
| 443/tcp            | HTTPS port                                       |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /conf.d            | Config data                                      |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

